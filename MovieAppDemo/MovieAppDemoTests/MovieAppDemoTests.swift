//
//  MovieAppDemoTests.swift
//  MovieAppDemoTests
//
//  Created by Ashish Agrawal on 02/03/19.
//  Copyright © 2019 Ashish Agrawal. All rights reserved.
//

import XCTest
@testable import MovieAppDemo

class MovieAppDemoTests: XCTestCase {

    var viewModel:ViewModelProtocol!
    override func setUp() {
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testJSONMapping() throws {
        
        viewModel = TestViewModel()
        viewModel.apiHit { (anyObject) in
            if let anyObject = anyObject {
                if let res = anyObject as? [String:AnyObject] , let array = res["results"]  as? [[String:AnyObject]]  {
                    if let movie: Movie = Movie.init(data: array[0]) {
                        XCTAssertNotEqual(movie.title, nil, "Title is nil")
                        XCTAssertNotEqual(movie.img, nil, "Image is nil")
                        if movie.rating == nil {
                            XCTFail("Rating is nil")
                        }
                    } else {
                        XCTFail("Data is not correct")
                    }
                } else if let str = anyObject as? String {
                    XCTFail(str)
                } else {
                    XCTFail("Data is not different")
                }
            } else {
                 XCTFail("Missing file: json File")
            }
        }
    }
}


// MARK: - Testing Extension

class TestViewModel:ViewModelProtocol {
    
    func apiHit(handler:@escaping (_ success:AnyObject?)->Void) {
        let bundle = Bundle(for: type(of: self))
        guard let url = bundle.url(forResource: "Demo", withExtension: "json") else {
           handler(nil)
            return
        }
        do {
            let json = try Data(contentsOf: url)
            let parseJson = try JSONSerialization.jsonObject(with: json, options: JSONSerialization.ReadingOptions.allowFragments)
            handler(parseJson as AnyObject)
        }catch {
            handler(error.localizedDescription as AnyObject)
        }
    }
}
