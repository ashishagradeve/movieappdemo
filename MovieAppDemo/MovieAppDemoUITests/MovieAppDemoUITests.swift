//
//  MovieAppDemoUITests.swift
//  MovieAppDemoUITests
//
//  Created by Ashish Agrawal on 02/03/19.
//  Copyright © 2019 Ashish Agrawal. All rights reserved.
//

import XCTest

class MovieAppDemoUITests: XCTestCase {

    var app:XCUIApplication!
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        app = XCUIApplication()
        
        app.launchArguments.append("--uitesting")

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testTabBar() {
        app.launch()

        XCTAssertTrue(app.alerts.element.waitForExistence(timeout: 5))
        app.alerts.buttons["Dismiss"].tap()
        
        let tabBarsQuery = app.tabBars
        tabBarsQuery.buttons["Popular"].tap()

        XCTAssertTrue(app.alerts.element.waitForExistence(timeout: 5))
        app.alerts.buttons["Dismiss"].tap()
        
        tabBarsQuery.buttons["Upcoming"].tap()
        
        XCTAssertTrue(app.alerts.element.waitForExistence(timeout: 5))
        app.alerts.buttons["Dismiss"].tap()
        
        tabBarsQuery.buttons["Now Playing"].tap()
        XCTAssertTrue(app.alerts.element.waitForExistence(timeout: 5))
        app.alerts.buttons["Dismiss"].tap()
        
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testTapOnCell() {
        
        app.launch()
        
        XCTAssertTrue(app.alerts.element.waitForExistence(timeout: 5))
        app.alerts.buttons["Dismiss"].tap()
        
        // tap on cell
        app.tables.cells.element(boundBy: 0).tap()
        
        XCTAssertTrue(app.isDisplayingOnDetailVC)
        XCTAssertTrue(app.alerts.element.waitForExistence(timeout: 5))
        app.alerts.buttons["Dismiss"].tap()

        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPullToRefresh() {
        
        app.launch()
        
        XCTAssertTrue(app.alerts.element.waitForExistence(timeout: 5))
        app.alerts.buttons["Dismiss"].tap()
        
        let table = app.tables.element(boundBy: 0)
        table.gentleSwipe(XCUIElement.direction.Down)
        
        
        XCTAssertTrue(app.alerts.element.waitForExistence(timeout: 5))
        app.alerts.buttons["Dismiss"].tap()
        
    }
    
}



//  MSRK:- XCUIApplication

extension XCUIApplication {
    var isDisplayingOnHomeVC: Bool {
        return otherElements["onHomeView"].exists
    }
    
    var isDisplayingOnDetailVC: Bool {
        return otherElements["onDetailView"].exists
    }
}


//  MSRK:- XCUIElement

extension XCUIElement {
    
    //  scroll to last
    func scrollToElement(element: XCUIElement) {
        while !element.visible() {
            gentleSwipe(direction.Up)
        }
    }
    
    func visible() -> Bool {
        guard self.exists && !self.frame.isEmpty else { return false }
        return XCUIApplication().windows.element(boundBy: 0).frame.contains(self.frame)
    }

    //  XCUIElement+GentleSwipe.swift

    enum direction : Int {
        case Up, Down, Left, Right
    }
    
    func gentleSwipe(_ direction : direction) {
        let half : CGFloat = 0.5
        let adjustment : CGFloat = 0.55
        let pressDuration : TimeInterval = 0.1
        
        let lessThanHalf = half - adjustment
        let moreThanHalf = half + adjustment
        
        let centre = self.coordinate(withNormalizedOffset: CGVector(dx: half, dy: half))
        let aboveCentre = self.coordinate(withNormalizedOffset: CGVector(dx: half, dy: lessThanHalf))
        let belowCentre = self.coordinate(withNormalizedOffset: CGVector(dx: half, dy: moreThanHalf))
        let leftOfCentre = self.coordinate(withNormalizedOffset: CGVector(dx: lessThanHalf, dy: half))
        let rightOfCentre = self.coordinate(withNormalizedOffset: CGVector(dx: moreThanHalf, dy: half))
        
        switch direction {
        case .Up:
            centre.press(forDuration: pressDuration, thenDragTo: aboveCentre)
            break
        case .Down:
            centre.press(forDuration: pressDuration, thenDragTo: belowCentre)
            break
        case .Left:
            centre.press(forDuration: pressDuration, thenDragTo: leftOfCentre)
            break
        case .Right:
            centre.press(forDuration: pressDuration, thenDragTo: rightOfCentre)
            break
        }
    }
}
