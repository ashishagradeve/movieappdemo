//
//  ViewModelProtocol.swift
//  MovieAppDemo
//
//  Created by Ashish Agrawal on 07/03/19.
//  Copyright © 2019 Ashish Agrawal. All rights reserved.
//

import UIKit

@objc protocol ViewModelProtocol {
    @objc optional func validation() -> String?
    func apiHit(handler:@escaping (_ success:AnyObject?)->Void)
}

extension ViewModelProtocol {
    
    typealias failure = (_ error:String?) ->Void
    typealias sucess = () ->Void
    
    func getErrorMessage(error:Error) -> String? {
        switch error {
        case ServerDefineError.networkNotAvailable:
            return "Check your network connection"
        case ServerDefineError.userCancelled:
            return nil
        case ServerDefineError.somthingWentWrong:
            return "Something went wrong"
        case ServerDefineError.serverGenerated(message:let message, code: _):
            return message
        default :
            if error.localizedDescription == "cancelled" {
                return nil
            }
            return error.localizedDescription
        }
    }
}
