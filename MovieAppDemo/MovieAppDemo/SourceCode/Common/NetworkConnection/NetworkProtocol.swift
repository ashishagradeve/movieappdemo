//
//  NetworkProtocol.swift
//  MovieAppDemo
//
//  Created by Ashish Agrawal on 02/03/19.
//  Copyright © 2019 Ashish Agrawal. All rights reserved.
//

import UIKit
protocol NetworkUrl {
    func getMethodType() -> API_httpMethod
    func getRawValue() -> String
    func getAPIContentType () -> API_Content_Type
}

// MARK:- API_httpMethod
enum API_httpMethod {
    
    case get
    case delete
    case post
    case put
    case patch(body: Data)
    
    var rawString: String {
        switch self {
        case .get:
            return "GET"
        case .delete:
            return "DELETE"
        case .post:
            return "POST"
        case .put://(_):
            return "PUT"
        case .patch(_):
            return "PATCH"
        }
    }
}

// MARK:- API_Content_Type
enum API_Content_Type: String {
    case json = "application/json"
    case x_www_form_urlencoded = "application/x-www-form-urlencoded"
    case formData = "multipart/form-data"
    case none = ""
}


// MARK:- ExtendedUrl
enum ExtendedUrl: NetworkUrl {
    case search
    case upcoming
    case nowPlaying
    case popular
    case movieDetail(String)
    case movieCredit(String)
    case movieVideo(String)
    
    func getRawValue() -> String {
        switch self {
        case .search:
            return "search/movie?"
        case .upcoming:
            return "movie/upcoming?"
        case .nowPlaying:
            return "movie/now_playing?"
        case .popular:
            return "movie/popular?"
        case .movieDetail(let id):
            return "movie/\(id)?"
        case .movieCredit(let id):
            return "movie/\(id)/credits?"
        case .movieVideo(let id):
            return "movie/\(id)/videos?"
        }
    }
    func getAPIContentType () -> API_Content_Type {
        return .x_www_form_urlencoded
    }
    func getMethodType() -> API_httpMethod{
        return .get
    }
}

enum ServerDefineError:Error {
    case serverGenerated(message:String?,code:Int?)
    case networkNotAvailable
    case userCancelled
    case somthingWentWrong
}
