//
//  NetworkImageDownload.swift
//  MovieAppDemo
//
//  Created by Ashish Agrawal on 02/03/19.
//  Copyright © 2019 Ashish Agrawal. All rights reserved.
//

import UIKit

class NetworkImageDownload: Operation {
    
    private let imageCache = NSCache<AnyObject, AnyObject>()
    
    private var baseUrl = "https://image.tmdb.org/t/p/w500"
    
    func getDownloadImageUrl(url:String? , image: @escaping (_ image:UIImage?) -> Void) {
        if let imageFromCache = imageCache.object(forKey: url as AnyObject) as? UIImage {
            image(imageFromCache)
            return
        }

        getImageWith(url) {(data, error) in
            DispatchQueue.main.async {
                if let data = data, let imageC = UIImage.init(data: data) {
                    image(imageC)
                    self.imageCache.setObject(imageC, forKey: url as AnyObject)
                } else {
                    image(nil)
                }
            }
        }
    }
    
    private func getImageWith(_ imgUrl: String?, handler: @escaping ((_ data: Data?, NSError?) -> Void))
    {
        if let imgUrl = imgUrl?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) , imgUrl.count > 0  , let url  = URL.init(string:baseUrl + imgUrl ) {
            
            let fileName = imgUrl.split(separator: "/").last
            
            
            let tempDirectory = URL(fileURLWithPath: NSTemporaryDirectory(), isDirectory: true)
            let finalPath = tempDirectory.appendingPathComponent(String(fileName!))
            
            let fileExists = FileManager.default.fileExists(atPath: finalPath.path)
            if (fileExists) {
                if let imgData = try? Data(contentsOf: URL(fileURLWithPath: finalPath.path)) {
                    handler(imgData, nil)
                }
                return
            } else {
                
                // if not, download image from url
                URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                    if let error = error {
                        print(error.localizedDescription)
                        handler(nil, error as NSError)
                        return
                    }
                    if let imgData = data , let _ = UIImage.init(data: imgData) {
                        do {
                            try imgData.write(to: finalPath)
                            handler(imgData, nil)
                        } catch {
                            print(error)
                        }
                    } else {
                        handler(nil, nil)
                    }
                }).resume()
            }
        }
    }
}
