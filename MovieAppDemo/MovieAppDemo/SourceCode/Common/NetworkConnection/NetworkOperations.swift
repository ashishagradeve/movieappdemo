//
//  NetworkOperations.swift
//  MovieAppDemo
//
//  Created by Ashish Agrawal on 02/03/19.
//  Copyright © 2019 Ashish Agrawal. All rights reserved.
//

import UIKit

struct APIKeys {
    static let api_key = "api_key"
    static let page = "page"
    static let query = "query"
}

class NetworkOperations<T:NetworkUrl>: Operation {
    
    // MARK:- variable
    private let baseUrl = "https://api.themoviedb.org/3/"
    private let api_key = "67b85ef82a178943907eadc3ca9df277"
    
    // MARK:- typealias
    typealias handler = (_ response:Any? , _ error:Error?,_ tag:String? , _ statusCode:Int?)->Void
    
    // MARK:- init
    private var tag:T
    private var handlers:handler?
    var task:URLSessionDataTask?
    
    // MARK:- init
    init(tag:T) {
        self.tag = tag
    }
    
    // MARK:- Method
    func data_request(parameter:[String:Any]?, handler:@escaping handler ) -> URLSessionDataTask? {
        
        if !isInternetAvailable() {
            handler(nil,ServerDefineError.networkNotAvailable,tag.getRawValue(),0)
            return nil
        }
        handlers = handler
        var para:[String:Any]! = parameter
        if para == nil {
            para = [:]
        }
        para[APIKeys.api_key] = api_key
        let urlString = baseUrl + tag.getRawValue() + stringFromHttpParameters(data: para)
        print(urlString)
        if let url:URL = URL(string:urlString) {
            let session = URLSession.shared
            var request = URLRequest(url: url)
            request.httpMethod = tag.getMethodType().rawString
            request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
            task = session.dataTask(with: request as URLRequest) {
                (data, response, error) in
                self.responseManagement(data: data, response: response, error: error)
            }
            task?.resume()
            return task
        }
        handler(nil,ServerDefineError.somthingWentWrong,tag.getRawValue(),0)
        return nil
    }
    
    private func responseManagement(data:Data?, response:URLResponse?, error:Error?) {
        let httpResponse = response as? HTTPURLResponse
        guard let data = data as Data?, let _:URLResponse = response, error == nil else {
            print("error")
            handlers?(nil,error,self.tag.getRawValue(),httpResponse?.statusCode)
            return
        }
        do {
            let parseJson = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments)
            print(parseJson)
            if let json = parseJson as? [String:Any] {
                if let errData = json["errors"] as? [String] , errData.count > 0 {
                    reponseType(nil,ServerDefineError.serverGenerated(message: errData[0], code: 0),self.tag.getRawValue(),httpResponse?.statusCode)
                } else {
                    reponseType(json,nil,self.tag.getRawValue(),httpResponse?.statusCode)
                }
            } else {
                reponseType(parseJson,nil,self.tag.getRawValue(),httpResponse?.statusCode)
            }
        }catch {
            print(error)
            let dataString = String(data: data, encoding: String.Encoding.utf8)
            print (dataString)
            reponseType(nil,error,self.tag.getRawValue(),httpResponse?.statusCode)
        }
    }
    
    private func reponseType(_ response:Any? , _ error:Error?,_ tag:String? , _ statusCode:Int?) {
        handlers?(response, error, tag,statusCode)
    }
}

// MARK:- Append Url

func stringFromHttpParameters(data:[String:Any]) -> String {
    
    let parametersString = convertJsonToString(data: data)
    return parametersString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
}

func convertJsonToString(data:[String:Any]) -> String {
    var parametersString = ""
    for (key, value) in data {
        parametersString = parametersString + key + "=" + "\(value)" + "&"
    }
    parametersString = parametersString.substring(to: parametersString.index(before: parametersString.endIndex))
    return parametersString
}
