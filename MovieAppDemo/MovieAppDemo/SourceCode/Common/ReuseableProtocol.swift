//
//  ReuseableProtocol.swift
//  MovieAppDemo
//
//  Created by Ashish Agrawal on 02/03/19.
//  Copyright © 2019 Ashish Agrawal. All rights reserved.
//

protocol ReuseableProtocol {
    static var reusableIdentifier:String {
        get
    }
}

extension ReuseableProtocol {
    static var reusableIdentifier:String {
        get {
            return "\(self)"
        }
    }
}
