//
//  Credits.swift
//  MovieAppDemo
//
//  Created by Ashish Agrawal on 04/03/19.
//  Copyright © 2019 Ashish Agrawal. All rights reserved.
//

import UIKit

class Credit {
    
    var id:String
    var character:String?
    var name:String?
    var img:String?
    
    // MARK: - Init
    
    init?(data:[String:AnyObject]?) {
        
        guard let data = data else {
            return nil
        }
        guard let id = data["credit_id"] as? String else {
            return nil
        }
        self.id = id
        self.character = data["character"] as? String
        self.name = data["name"] as? String
        self.img = data["profile_path"] as? String
    }
}
