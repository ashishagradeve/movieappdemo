//
//  MovieDetail.swift
//  MovieAppDemo
//
//  Created by Ashish Agrawal on 04/03/19.
//  Copyright © 2019 Ashish Agrawal. All rights reserved.
//

import UIKit

class MovieDetail: Movie {
    
    var genres:String = ""
    var language:String = ""
    var overview:String?
    var releaseDate:String?
    var runtime:AnyObject?
    
    // MARK: - Init
    override init(movie:Movie) {
        super.init(movie:movie)
    }
    
    override init?(data: [String : AnyObject]?) {
        super.init(data: data)
        
        overview = data?["overview"] as? String
        releaseDate = data?["release_date"] as? String
        runtime = data?["runtime"]
        
        if let genres = data?["genres"] as? [[String:AnyObject]] {
            for value in genres {
                if let name = value["name"] as? String {
                    if self.genres.count > 0 {
                        self.genres = self.genres + ", "
                    }
                    self.genres = self.genres + name
                }
            }
        }
        
        if let languages = data?["spoken_languages"] as? [[String:AnyObject]] {
            for value in languages {
                if let name = value["name"] as? String {
                    if self.genres.count > 0 {
                        self.language = self.language + ", "
                    }
                    self.language = self.language + name + " "
                }
            }
        }
        
    }
}
