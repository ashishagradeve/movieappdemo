//
//  Movie.swift
//  MovieAppDemo
//
//  Created by Ashish Agrawal on 02/03/19.
//  Copyright © 2019 Ashish Agrawal. All rights reserved.
//

import UIKit

class Movie {

    var id:Int
    var title:String?
    var img:String?
    var rating:AnyObject?
    
    // MARK: - Init
    init(movie:Movie) {
        id = movie.id
        img = movie.img
        title = movie.title
        rating = movie.rating
    }
    
    init?(data:[String:AnyObject]?) {
        
        guard let data = data else {
            return nil
        }
        guard let id = data["id"] as? Int else {
            return nil
        }
        self.id = id
        self.title = data["original_title"] as? String
        self.rating = data["vote_average"] 
//        self.img = data["poster_path"] as? String
        self.img = data["backdrop_path"] as? String

    }
}
