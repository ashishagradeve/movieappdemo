//
//  Video.swift
//  MovieAppDemo
//
//  Created by Ashish Agrawal on 04/03/19.
//  Copyright © 2019 Ashish Agrawal. All rights reserved.
//

import UIKit

class Video: NSObject {

    var id:String
    var name:String?
    var key:String?
    
    // MARK: - Init
    
    init?(data:[String:AnyObject]?) {
        
        guard let data = data else {
            return nil
        }
        guard let id = data["id"] as? String else {
            return nil
        }
        self.id = id
        self.name = data["name"] as? String
        self.key = data["key"] as? String
    }
}
