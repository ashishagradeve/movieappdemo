//
//  HomeVC.swift
//  MovieAppDemo
//
//  Created by Ashish Agrawal on 02/03/19.
//  Copyright © 2019 Ashish Agrawal. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {

    // MARK: Outlets
    @IBOutlet weak var tbleView: UITableView!
    @IBOutlet weak var tabbr: UITabBar!

    // MARK: Variable
    private var viewActivity: UIView? {
        didSet {
            oldValue?.removeFromSuperview()
        }
    }
    private var refreshControl: UIRefreshControl!

    // MARK: Contants
    private let viewModel  = HomeViewModel()
    private let searchViewModel  = HomeViewModel()
    private let searchController = UISearchController(searchResultsController: nil)

    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.accessibilityIdentifier = "onHomeView"        
        configure()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Methods
    private func configure () {
        self.title = "Home"
        NotificationCenter.default.addObserver(self, selector: #selector(notificationListUpdate(_:)), name:  NSNotification.Name(rawValue: HomeViewModel.updateListNotificationName), object: nil)
        addSearchController()
        tbleView.estimatedRowHeight = 200
        tbleView.rowHeight = UITableView.automaticDimension
        addRefreshController()

        apiHit()
        tabbr.selectedItem = tabbr.items![0]
        tbleView.register(UINib.init(nibName: HomeTVCell.reusableIdentifier, bundle: Bundle.main), forCellReuseIdentifier: HomeTVCell.reusableIdentifier)
        
        
    }
    
    private func addRefreshController() {
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(apiHit), for: .valueChanged)
        tbleView.addSubview(refreshControl) // not required when using UITableViewController
    }
    
    @objc private func apiHit() {
        self.viewModel.refresh = true
        if ((isFiltering()) ? searchViewModel.refresh : viewModel.refresh)  == true {
            viewActivity = createActivityIndicator(view: self.view)
        }
        viewModel.apiHit {[weak self] (success) in
            DispatchQueue.main.async {
                
                if let weakS = self {
                    if CommandLine.arguments.contains("--uitesting") {
                        if weakS.isFiltering() {
                            showAlertMessage(string: "Search", viewController: nil)
                        } else {
                            showAlertMessage(string: "Pagination \(weakS.viewModel.selectedTab.rawValue)", viewController: nil)
                        }
                    }
                }
                
                self?.refreshControl.endRefreshing()
                removeActivityIndicatorFromSuperView(activity: self?.viewActivity)
            }
        }
    }
    
    private func addSearchController() {
        // Setup the Search Controller
        //        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search"
        searchController.searchBar.accessibilityIdentifier = "SearchBar"
        navigationItem.searchController = searchController
        definesPresentationContext = true
        
        // Setup the Scope Bar
        searchController.searchBar.delegate = self
    }
    
    private func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    private func isFiltering() -> Bool {
        let searchBarScopeIsFiltering = searchController.searchBar.selectedScopeButtonIndex != 0
        return searchController.isActive && (searchBarScopeIsFiltering || searchViewModel.searchText != nil )
    }
    
    private func whenSearching(isSearching:Bool) {
        searchViewModel.reset()
        tbleView.reloadData()
    }
    
    // MARK: - Notification
    @objc private  func notificationListUpdate(_ notification:Notification) {
        if let cellCount = notification.userInfo?[HomeViewModel.updateListNotificationName] as? Int {
            DispatchQueue.main.async {
                if cellCount > 0 {
                    self.addCell(abs(cellCount))
                } else if cellCount < 0 {
                    self.removeCell(abs(cellCount))
                }
            }
        }
    }
    
   private  func addCell(_ no:Int) {
        removeActivityIndicatorFromSuperView(activity: self.viewActivity)
        if no > 0 {
            tbleView.beginUpdates()
            var indexPaths = [IndexPath]()
            let count = (isFiltering()) ? searchViewModel.listMovies.count : viewModel.listMovies.count
            for row in (count - no)..<count {
                indexPaths.append(IndexPath.init(row: row, section: 0))
            }
            tbleView.insertRows(at: indexPaths, with: .none)
            tbleView.endUpdates()
        }
    }
    
   private  func removeCell(_ no:Int) {
        if viewModel.refresh == true {
            viewActivity = createActivityIndicator(view: self.view)
        }
        tbleView.reloadData()
    }
    
    // MARK: - IBAction
    

    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if let identifier = segue.identifier {
            switch identifier {
            case HomeViewModel.Segue.moveToDetail.rawValue:
                if let destination = segue.destination as? DetailVC , let index = sender as? Int {
                    let movieDetail = MovieDetail.init(movie: isFiltering() ? searchViewModel.listMovies[index] : viewModel.listMovies[index])
                    destination.setUpdate(movie: movieDetail)
                }
            default:
                break
            }
        }
    }
}

// MARK: - UITableViewDataSource
extension HomeVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = (isFiltering()) ? searchViewModel.listMovies.count : viewModel.listMovies.count
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: HomeTVCell.reusableIdentifier, for: indexPath) as! HomeTVCell
        cell.cellUpdate(movie: (isFiltering()) ? searchViewModel.listMovies[indexPath.row] : viewModel.listMovies[indexPath.row])
        return cell
    }
}

// MARK: - UITableViewDelegate
extension HomeVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: HomeViewModel.Segue.moveToDetail.rawValue , sender: indexPath.row)
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (isFiltering()) {
            searchViewModel.fetchMoreData(row: indexPath.row, handler: { (succuss) in })
        } else {
            viewModel.fetchMoreData(row: indexPath.row, handler: { (succuss) in })
        }
    }
}

// MARK: - UISearchBar Delegate

extension HomeVC: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("check searchBarSearchButtonClicked")
        if let search  = searchBar.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) , search.count > 0 {
            searchViewModel.searchText = search
            whenSearching(isSearching: true)
            tbleView.setContentOffset(CGPoint.zero, animated: false)
            searchViewModel.apiHit { (success) in}
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        print("check searchBarShouldBeginEditing")
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchViewModel.searchReset()
        tbleView.setContentOffset(CGPoint.zero, animated: false)
        tbleView.reloadData()
        print("check searchBarCancelButtonClicked")
    }
}


// MARK: - UITabBarDelegate

extension HomeVC: UITabBarDelegate {

    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        let indexOfTab = tabBar.items?.index(of: item)
        if indexOfTab == 0 {
            viewModel.selectedTab = .nowPlaying
        } else if indexOfTab == 1 {
            viewModel.selectedTab = .popular
        } else if indexOfTab == 2 {
            viewModel.selectedTab = .upcoming
        }
    }
}
