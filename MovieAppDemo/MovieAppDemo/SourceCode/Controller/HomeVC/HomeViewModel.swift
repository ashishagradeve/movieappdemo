//
//  HomeViewModel.swift
//  MovieAppDemo
//
//  Created by Ashish Agrawal on 02/03/19.
//  Copyright © 2019 Ashish Agrawal. All rights reserved.
//

import UIKit

enum HomeTabs:String {
    case upcoming = "upcoming"
    case nowPlaying = "now_playing"
    case popular = "popular"
}

class HomeViewModel {
    
    enum Segue:String {
        case moveToDetail = "moveToDetail"
    }
    
    static let updateListNotificationName = "updateList"
    static let listOfAllSearchText = "AllSearchText"
    
    var listMovies = [Movie]()
    var page = 1
    var noMoreData = false
    var apiHit = false
    var searchText:String?
    var refresh = true
    var task:URLSessionDataTask? {
        didSet{
            oldValue?.cancel()
        }
    }
    
    var selectedTab: HomeTabs = .nowPlaying {
        didSet{
            if oldValue != selectedTab {
                refresh  = true
                reset()
                commonApiHit()
            }
        }
    }
    
    
    func commonApiHit() {
        apiHit {[weak self] (success) in
            
            if let weakS = self {
                if CommandLine.arguments.contains("--uitesting") {
                    if weakS.page == 1 {
                        if (weakS.searchText != nil) {
                            showAlertMessage(string: "Search", viewController: nil)
                        } else {
                            showAlertMessage(string: "Pagination \(weakS.selectedTab.rawValue)", viewController: nil)
                        }
                    }
                }
            }
        }
    }
    
    func reset() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: HomeViewModel.updateListNotificationName), object:self , userInfo: [HomeViewModel.updateListNotificationName:-listMovies.count])
        listMovies = [Movie]()
        page = 1
        noMoreData = false
        apiHit = false
    }
    
    func searchReset() {
        searchText = nil
        reset()
    }
    
}

//MARK: API
extension HomeViewModel:ViewModelProtocol {
    
    //MARK: PAGINATION
    func fetchMoreData(row:Int, handler:@escaping (_ success:Bool)->Void) {
        if row >= listMovies.count - 2 && noMoreData == false && apiHit == false {
            commonApiHit()
        }
    }
    
    //MARK: API
    func apiHit(handler:@escaping (_ success:AnyObject?)->Void) {
        
        if refresh == false {
            guard apiHit == false && noMoreData == false else {
                return
            }
        }
        
        apiHit = true
        
        var tag:ExtendedUrl!
        var para:[String:Any] = [APIKeys.page:page]
        if let search = searchText {
            tag = ExtendedUrl.search
            para[APIKeys.query] = search
        } else {
            switch (selectedTab) {
            case .nowPlaying:
                tag = ExtendedUrl.nowPlaying
            case .upcoming:
                tag = ExtendedUrl.upcoming
            case .popular:
                tag = ExtendedUrl.popular
            }
        }
        
        task = NetworkOperations.init(tag: tag).data_request(parameter: para) { [weak self](res, error, tag, statusCode) in
            
            if let error = error  {
                if error.localizedDescription != "cancelled" {
                    DispatchQueue.main.async {
                        showToast(message: error.localizedDescription)
                    }
                }
                handler(nil)
            } else if let res = res as? [String:AnyObject] , let doc = res["results"]  as? [[String:AnyObject]]  {
                
                if doc.count > 0 {
                    for value in doc {
                        if let Movie = Movie.init(data: value) {
                            self?.listMovies.append(Movie)
                        }
                    }
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: HomeViewModel.updateListNotificationName), object:self , userInfo: [HomeViewModel.updateListNotificationName:doc.count])
                    
                } else {
                    self?.noMoreData = true
                }
                
                if let page = res["page"]  as? Int {
                    self?.page = page + 1
                } else {
                    self?.page += 1
                }
                handler(self?.listMovies as AnyObject)
            } else if let res = res as? [String:AnyObject] , let status_message = res["status_message"]  as? String {
                DispatchQueue.main.async {
                    showToast(message: status_message)
                }
                handler(nil)
            }
            
            self?.refresh = false
            self?.apiHit = false
        }
    }
}
