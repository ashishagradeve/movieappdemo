//
//  VideoVC.swift
//  MovieAppDemo
//
//  Created by Ashish Agrawal on 04/03/19.
//  Copyright © 2019 Ashish Agrawal. All rights reserved.
//

import UIKit

class VideoVC: UIViewController {

    @IBOutlet weak var tbleView: UITableView!
    
    // MARK: Variable
    private var viewActivity: UIView? {
        didSet {
            oldValue?.removeFromSuperview()
        }
    }
    private var refreshControl: UIRefreshControl!
    
    // MARK: Contants
    private let viewModel  = VideoVewModel()
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Methods
    
    func setUpdate(movie:MovieDetail) {
        viewModel.movieDetail = movie
    }
    
    // MARK: - Private Methods
    private func configure () {
        self.title = "Videos"
        tbleView.estimatedRowHeight = 200
        tbleView.rowHeight = UITableView.automaticDimension
        addRefreshController()
        tbleView.register(UINib.init(nibName: VideoTVCell.reusableIdentifier, bundle: Bundle.main), forCellReuseIdentifier: VideoTVCell.reusableIdentifier)
        apiHit()
        
    }
    
    private func addRefreshController() {
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(apiHit), for: .valueChanged)
        tbleView.addSubview(refreshControl) // not required when using UITableViewController
    }
    
    @objc private func apiHit() {
        viewActivity = createActivityIndicator(view: self.view)
        
        viewModel.apiHit {[weak self] (success) in
            DispatchQueue.main.async {
                self?.tbleView.reloadData()
                self?.refreshControl.endRefreshing()
                removeActivityIndicatorFromSuperView(activity: self?.viewActivity)
            }
        }
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if let identifier = segue.identifier {
            switch identifier {
            case VideoVewModel.Segue.moveToVideo.rawValue:
                if let destination = segue.destination as? PlayVideoVC , let row = sender as? Int{
                    destination.video = viewModel.listOfVideos[row]
                }
            default:
                break
            }
        }
    }
}

// MARK: - UITableViewDataSource
extension VideoVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.listOfVideos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: VideoTVCell.reusableIdentifier, for: indexPath) as! VideoTVCell
        cell.cellUpdate(video: viewModel.listOfVideos[indexPath.row])
        return cell
    }
}

// MARK: - UITableViewDelegate
extension VideoVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: VideoVewModel.Segue.moveToVideo.rawValue , sender: indexPath.row)
    }
}
