//
//  VideoVewModel.swift
//  MovieAppDemo
//
//  Created by Ashish Agrawal on 04/03/19.
//  Copyright © 2019 Ashish Agrawal. All rights reserved.
//

import UIKit

class VideoVewModel: NSObject {

    enum Segue:String {
        case moveToVideo = "moveToVideo"
    }
    
    var task:URLSessionDataTask? {
        didSet{
            oldValue?.cancel()
        }
    }
    
    var listOfVideos:[Video] = []
    var movieDetail:MovieDetail!
    
}

//MARK: API
extension VideoVewModel:ViewModelProtocol {
    
    //MARK: API
    func apiHit(handler:@escaping (_ success:AnyObject?)->Void) {
        
        task = NetworkOperations.init(tag: ExtendedUrl.movieVideo("\(movieDetail.id)")).data_request(parameter: [:]) { [weak self](res, error, tag, statusCode) in
            
            if let error = error  {
                if error.localizedDescription != "cancelled" {
                    DispatchQueue.main.async {
                        showToast(message: error.localizedDescription)
                    }
                }
            } else if let res = res as? [String:AnyObject]   , let casts = res["results"]  as? [[String:AnyObject]]  {
                self?.listOfVideos = []
                for value in casts {
                    if let cast = Video.init(data: value) {
                        self?.listOfVideos.append(cast)
                    }
                }
                handler(self?.listOfVideos as AnyObject)
                return
            } else if let res = res as? [String:AnyObject] , let status_message = res["status_message"]  as? String {
                DispatchQueue.main.async {
                    showToast(message: status_message)
                }
            }
            handler(nil)
        }
    }
}

