//
//  DetailViewModel.swift
//  MovieAppDemo
//
//  Created by Ashish Agrawal on 02/03/19.
//  Copyright © 2019 Ashish Agrawal. All rights reserved.
//

import UIKit

class DetailViewModel: NSObject {
    
    enum Segue:String {
        case moveToCredit = "moveToCredit"
        case moveToVideo = "moveToVideo"
    }
    
    let title = ["Release date","Running time","Original Languages","Genres","Synopsis","List of actor/actress","Videos"]
    var task:URLSessionDataTask? {
        didSet{
            oldValue?.cancel()
        }
    }
    
    var movie:MovieDetail!
    
    func setUpdateTitle(indexPath:IndexPath) -> String {
        return title[indexPath.row - 1]
    }

    func setUpdateDetail(indexPath:IndexPath) -> String? {
        switch indexPath.row {
        case 1:
            return movie.releaseDate
        case 2:
            return "\(movie.runtime ?? 0 as AnyObject) mins"
        case 3:
            return movie.language
        case 4:
            return movie.genres
        case 5:
            return movie.overview
        case 6:
            return ""
        default:
            return ""
        }
    }
}

//MARK: API
extension DetailViewModel:ViewModelProtocol {
    
    //MARK: API
    func apiHit(handler:@escaping (_ success:AnyObject?)->Void) {
        
        task = NetworkOperations.init(tag: ExtendedUrl.movieDetail("\(movie.id)")).data_request(parameter: [:]) { [weak self](res, error, tag, statusCode) in
            
            if let error = error  {
                if error.localizedDescription != "cancelled" {
                    DispatchQueue.main.async {
                        showToast(message: error.localizedDescription)
                    }
                }
            } else if let res = res as? [String:AnyObject]  {
                self?.movie = MovieDetail.init(data: res)
                handler(self?.movie as AnyObject)
                return
            } else if let res = res as? [String:AnyObject] , let status_message = res["status_message"]  as? String {
                DispatchQueue.main.async {
                    showToast(message: status_message)
                }
            }
            handler(nil)

        }
    }
}
