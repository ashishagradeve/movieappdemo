//
//  DetailVC.swift
//  MovieAppDemo
//
//  Created by Ashish Agrawal on 02/03/19.
//  Copyright © 2019 Ashish Agrawal. All rights reserved.
//

import UIKit

class DetailVC: UIViewController {

    @IBOutlet weak var tbleView: UITableView!
    
    // MARK: Variable
    private var viewActivity: UIView? {
        didSet {
            oldValue?.removeFromSuperview()
        }
    }
    private var refreshControl: UIRefreshControl!
    
    // MARK: Contants
    private let viewModel  = DetailViewModel()
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.accessibilityIdentifier = "onDetailView"

        configure()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Methods
    
    func setUpdate(movie:MovieDetail) {
        viewModel.movie = movie
    }
    
    // MARK: - Private Methods
    private func configure () {
        self.title = "Detail"
        tbleView.estimatedRowHeight = 200
        tbleView.rowHeight = UITableView.automaticDimension
        addRefreshController()
        tbleView.register(UINib.init(nibName: HomeTVCell.reusableIdentifier, bundle: Bundle.main), forCellReuseIdentifier: HomeTVCell.reusableIdentifier)
        tbleView.register(UINib.init(nibName: DetailTVCell.reusableIdentifier, bundle: Bundle.main), forCellReuseIdentifier: DetailTVCell.reusableIdentifier)
        
        apiHit()
        
    }
    
    private func addRefreshController() {
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(apiHit), for: .valueChanged)
        tbleView.addSubview(refreshControl) // not required when using UITableViewController
    }
    
    @objc private func apiHit() {
        viewActivity = createActivityIndicator(view: self.view)

        viewModel.apiHit {[weak self] (success) in
            DispatchQueue.main.async {
                
                if let weakS = self {
                    if CommandLine.arguments.contains("--uitesting") {
                        showAlertMessage(string: "\(weakS.viewModel.movie.title)", viewController: nil)
                    }
                }
                
                self?.tbleView.reloadData()
                self?.refreshControl.endRefreshing()
                removeActivityIndicatorFromSuperView(activity: self?.viewActivity)
            }
        }
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if let identifier = segue.identifier {
            switch identifier {
            case DetailViewModel.Segue.moveToCredit.rawValue:
                if let destination = segue.destination as? CreditVC {
                    destination.setUpdate(movie: viewModel.movie)
                }
            case DetailViewModel.Segue.moveToVideo.rawValue:
                if let destination = segue.destination as? VideoVC {
                    destination.setUpdate(movie: viewModel.movie)
                }
            default:
                break
            }
        }
    }
}

// MARK: - UITableViewDataSource
extension DetailVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.title.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: HomeTVCell.reusableIdentifier, for: indexPath) as! HomeTVCell
            cell.cellUpdate(movie: viewModel.movie)
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: DetailTVCell.reusableIdentifier, for: indexPath) as! DetailTVCell
        if indexPath.row == 6 || indexPath.row == 7 {
            cell.accessoryType = UITableViewCell.AccessoryType.disclosureIndicator
        } else {
            cell.accessoryType = UITableViewCell.AccessoryType.none
        }
        cell.cellDetail(title: viewModel.setUpdateTitle(indexPath: indexPath), detail: viewModel.setUpdateDetail(indexPath: indexPath) ?? "")
        return cell
    }
}

// MARK: - UITableViewDelegate
extension DetailVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 6 {
            self.performSegue(withIdentifier: DetailViewModel.Segue.moveToCredit.rawValue , sender: indexPath.row)
        } else if indexPath.row == 7 {
            self.performSegue(withIdentifier: DetailViewModel.Segue.moveToVideo.rawValue , sender: indexPath.row)
        }
    }
}
