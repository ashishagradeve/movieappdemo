//
//  CreditViewController.swift
//  MovieAppDemo
//
//  Created by Ashish Agrawal on 04/03/19.
//  Copyright © 2019 Ashish Agrawal. All rights reserved.
//

import UIKit

class CreditVC: UIViewController {

    @IBOutlet weak var tbleView: UITableView!
    
    // MARK: Variable
    private var viewActivity: UIView? {
        didSet {
            oldValue?.removeFromSuperview()
        }
    }
    private var refreshControl: UIRefreshControl!
    
    // MARK: Contants
    private let viewModel  = CreditViewModel()
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Methods
    
    func setUpdate(movie:MovieDetail) {
        viewModel.movieDetail = movie
    }
    
    // MARK: - Private Methods
    private func configure () {
        self.title = "Credits"
        tbleView.estimatedRowHeight = 200
        tbleView.rowHeight = UITableView.automaticDimension
        addRefreshController()
        tbleView.register(UINib.init(nibName: CreditTVCell.reusableIdentifier, bundle: Bundle.main), forCellReuseIdentifier: CreditTVCell.reusableIdentifier)
        
        apiHit()
        
    }
    
    private func addRefreshController() {
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(apiHit), for: .valueChanged)
        tbleView.addSubview(refreshControl) // not required when using UITableViewController
    }
    
    @objc private func apiHit() {
        viewActivity = createActivityIndicator(view: self.view)
        
        viewModel.apiHit {[weak self] (success) in
            DispatchQueue.main.async {
                self?.tbleView.reloadData()
                self?.refreshControl.endRefreshing()
                removeActivityIndicatorFromSuperView(activity: self?.viewActivity)
            }
        }
    }
}

// MARK: - UITableViewDataSource
extension CreditVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (viewModel.listOfCredits.count > 5) ? 5: viewModel.listOfCredits.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CreditTVCell.reusableIdentifier, for: indexPath) as! CreditTVCell
        cell.cellUpdate(credit: viewModel.listOfCredits[indexPath.row])
        return cell
    }
}

// MARK: - UITableViewDelegate
extension CreditVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
}
