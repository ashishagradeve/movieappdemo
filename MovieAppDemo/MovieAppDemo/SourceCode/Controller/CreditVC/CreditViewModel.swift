//
//  CreditVewModel.swift
//  MovieAppDemo
//
//  Created by Ashish Agrawal on 04/03/19.
//  Copyright © 2019 Ashish Agrawal. All rights reserved.
//

import UIKit

class CreditViewModel: NSObject {

    var task:URLSessionDataTask? {
        didSet{
            oldValue?.cancel()
        }
    }
    
    var listOfCredits:[Credit] = []
    var movieDetail:MovieDetail!
    
}

//MARK: API
extension CreditViewModel:ViewModelProtocol {
    
    //MARK: API
    func apiHit(handler:@escaping (_ success:AnyObject?)->Void) {
        
        task = NetworkOperations.init(tag: ExtendedUrl.movieCredit("\(movieDetail.id)")).data_request(parameter: [:]) { [weak self](res, error, tag, statusCode) in
            
            if let error = error  {
                if error.localizedDescription != "cancelled" {
                    DispatchQueue.main.async {
                        showToast(message: error.localizedDescription)
                    }
                }
            } else if let res = res as? [String:AnyObject]   , let casts = res["cast"]  as? [[String:AnyObject]]  {
                self?.listOfCredits = []
                for value in casts {
                    if let cast = Credit.init(data: value) {
                        self?.listOfCredits.append(cast)
                    }
                }
                handler(self?.listOfCredits as AnyObject)
                return
            } else if let res = res as? [String:AnyObject] , let status_message = res["status_message"]  as? String {
                DispatchQueue.main.async {
                    showToast(message: status_message)
                }
            }
            handler(nil)
            
        }
    }
}

