//
//  PlayVideoVC.swift
//  MovieAppDemo
//
//  Created by Ashish Agrawal on 04/03/19.
//  Copyright © 2019 Ashish Agrawal. All rights reserved.
//

import UIKit
import WebKit

class PlayVideoVC: UIViewController {

    @IBOutlet weak var wv: WKWebView!
    
    // MARK: Variable
    var video: Video!
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        loadYoutube()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadYoutube() {
        // create a custom youtubeURL with the video ID
        guard let key = video.key else {
            showAlertMessage(string: "No Video found", viewController: nil)
            return
        }
        // https://www.youtube.com/watch?v=SUXWAEX2jlg
        // https://www.youtube.com/embed/\(key)
        guard let youtubeURL = URL(string: "https://www.youtube.com/watch?v=\(key)") else {
            showAlertMessage(string: "Video not found", viewController: nil)
            return
        }
        // load your web request
        wv.load( URLRequest(url: youtubeURL) )
    }
    
}
