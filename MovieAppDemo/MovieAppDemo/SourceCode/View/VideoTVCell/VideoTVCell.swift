//
//  VideoTVCell.swift
//  MovieAppDemo
//
//  Created by Ashish Agrawal on 04/03/19.
//  Copyright © 2019 Ashish Agrawal. All rights reserved.
//

import UIKit

class VideoTVCell: UITableViewCell, ReuseableProtocol {

//    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func cellUpdate(video:Video) {
        lblTitle.text = video.name
//        self.img.image = nil
//        NetworkImageDownload.init().getDownloadImageUrl(url: video.img) { [weak self] (image) in
//            self?.img.image = image
//        }
    }
}
