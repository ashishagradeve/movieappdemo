//
//  DetailTVCell.swift
//  MovieAppDemo
//
//  Created by Ashish Agrawal on 04/03/19.
//  Copyright © 2019 Ashish Agrawal. All rights reserved.
//

import UIKit

class DetailTVCell: UITableViewCell, ReuseableProtocol {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet var lblDetail: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
        // Configure the view for the selected state
    
    func cellDetail(title:String , detail:String) {
        lblTitle.text = title
        lblDetail.text = detail
    }
}
