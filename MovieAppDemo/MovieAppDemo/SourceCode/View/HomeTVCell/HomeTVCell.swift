//
//  HomeTVCell.swift
//  MovieAppDemo
//
//  Created by Ashish Agrawal on 02/03/19.
//  Copyright © 2019 Ashish Agrawal. All rights reserved.
//

import UIKit

class HomeTVCell: UITableViewCell, ReuseableProtocol {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet var lblRating: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func cellUpdate(movie:Movie) {
        lblRating.text = "\(movie.rating ?? 0 as AnyObject)"
        lblTitle.text = movie.title
        self.img.image = nil
        NetworkImageDownload.init().getDownloadImageUrl(url: movie.img) { [weak self] (image) in
            self?.img.image = image
        }
    }
    
    func cellUpdate(movie:MovieDetail) {
        lblRating.text = "\(movie.rating ?? 0 as AnyObject)"
        lblTitle.text = movie.title
        self.img.image = nil
        NetworkImageDownload.init().getDownloadImageUrl(url: movie.img) { [weak self] (image) in
            self?.img.image = image
        }
    }
}
