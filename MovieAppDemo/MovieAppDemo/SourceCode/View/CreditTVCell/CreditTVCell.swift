//
//  CreditTVCell.swift
//  MovieAppDemo
//
//  Created by Ashish Agrawal on 04/03/19.
//  Copyright © 2019 Ashish Agrawal. All rights reserved.
//

import UIKit

class CreditTVCell: UITableViewCell, ReuseableProtocol {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblCharacter: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func cellUpdate(credit:Credit) {
        lblTitle.text = credit.name
        lblCharacter.text = credit.character
        self.img.image = nil
        NetworkImageDownload.init().getDownloadImageUrl(url: credit.img) { [weak self] (image) in
            self?.img.image = image
        }
    }
}
